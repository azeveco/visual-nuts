const requestedCountries = [
  {
  country: "US",
  languages: ["en"]
  },
  {
  country: "BE",
  languages: ["nl","fr","de"]
  },
  {
  country: "NL",
  languages: ["nl","fy"]
  },
  {
  country: "DE",
  languages: ["de"]
  },
  {
  country: "ES",
  languages: ["es"]
  },
];

function getTopCountry(countries, language) {
  if(language) {
    const countriesThatSpeakLanguage = countries.filter(c => c.languages.includes(language));

    const topCountry = countriesThatSpeakLanguage.reduce((p, c, i, a) => a[p].languages.length > c.languages.length ? p : i, 0);

    return countriesThatSpeakLanguage[topCountry].country;
  } else {
    const topCountry = countries.reduce((p, c, i, a) => a[p].languages.length > c.languages.length ? p : i, 0);

    return countries[topCountry].country;
  }
}

function getSpokenLanguagesNumber(countries) {
  let spokenLanguages = [];

  countries.map(c => spokenLanguages.push(...c.languages));

  const spokenLanguagesFiltered = spokenLanguages.filter((v, i) => spokenLanguages.indexOf(v) == i);

  return spokenLanguagesFiltered.length
}

function getMostCommonLanguages(countries) {
  let languages = [];

  countries.map(c => languages.push(...c.languages));

  let occurrences = languages.reduce((a, c) => {
    a[c] = (a[c] || 0) + 1;
    return a;
  }, {});

  const maxOccurrencesNumber = Math.max(...Object.values(occurrences));
  const mostFrequentLanguages = Object.keys(occurrences).filter(k => occurrences[k] === maxOccurrencesNumber);

  return mostFrequentLanguages;
}

function countriesInfos(countries) {
  console.log(`Number of countries in the world: ${countries.length}`);
  console.log(`Country with the most official languages where they officially speak German (de): ${getTopCountry(countries, "de")}`);
  console.log(`Number of official languages spoken: ${getSpokenLanguagesNumber(countries)}`);
  console.log(`Country with the highest number of official languages: ${getTopCountry(countries)}`);
  console.log(`Most common official language(s) of all countries: ${getMostCommonLanguages(countries)}`);
}

countriesInfos(requestedCountries);
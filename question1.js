function visualNuts(number) {
  for(let i = 1; i <= number; i++) {
    if(i % 3 == 0 && i % 5 == 0) {
      console.log("Visual Nuts");
    } else if(i % 3 == 0) {
      console.log("Visual");
    } else if(i % 5 == 0) {
      console.log("Nuts")
    } else {
      console.log(i);
    }
  }
}

while (true) {
  const requestedNumber = parseInt(prompt("Type a number: "));
  
  if (requestedNumber != null && Number.isInteger(requestedNumber)) {
    visualNuts(requestedNumber);
    break;
  } else {
    console.log("The number must be an integer.");
  }
}